*** Settings ***
Library   Browser

*** Test Cases ***
Example Test
    New Browser     chromium
    New Page    https://frontend-njp.apps-crc.testing/
    ${page_source}  Get Page Source
    Should Contain  ${page_source}  Hello World!
