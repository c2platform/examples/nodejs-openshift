FROM    node:alpine3.17
COPY	package.json /src/package.json
RUN		cd /src; npm install --production
COPY	. /src
EXPOSE  3000
CMD		["node", "/src/server.js"]
