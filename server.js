
var express = require('express');
var app = express();
var fs = require('fs');
var package = JSON.parse(fs.readFileSync('/src/package.json'));

app.get('/', function (req, res) {
  res.send('Hello World! Version: ' + package.version);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
